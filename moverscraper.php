<?php


// Mover Scraper script written by Bill Pascoe, 2015.
// This scraper is intended for archiving or migrating your site or subsite.
// It was initially developed to retrieve an early digital humanities sites containing history texts and images from the University of Newcastle website to be archived with the library.
// This scraper allows you to get everything linked to from a given page under that page's domain and directory.
// It also allows for replacing portions of links so the site links will still work locally, or uploaded to a new location (such as a Library archive subsite).
// Links off site are not collected to avoid scraping the entire internet.
// It includes images, css and js files. You may need to tweak it to get other types of files.
// You may need to tweak it to get it to do exactly what you want it to do.
// There are probably some code improvements, but I ran out of time to perfect it.

$starturl = 'http://www-test.newcastle.edu.au/school/hss/research/publications/the-wellington-valley-project/';
$outputPath = "./output";
$newwebbasepath = "/library/cultural%20collections/the-wellington-valley-project/";
$cleanedlocalpath = "./cleaned";
$allmatches[$starturl] = getLocalPath($starturl);
$offsites = array();
$originalToLocal = array();
$localToAbsolute = array();

recursePages($starturl);

loopHash($originalToLocal);

pause();

cleanLinks();

//var_dump($allmatches);
    
echo "DONE";

// the pause here is so you can loop through a hash and watch as it goes, to look for issues etc.
function loopHash($thehash) {
    foreach ($thehash as $key => $value) {
    echo "\n KEY ~ VAL: ". $key . " ~ " . $value . "\n";
    if (!(strpos($p, "."))){ 
        pause();
    }
}
}

// Do all the work of collecting pages from links recursively
function recursePages($url){
    
    global $starturl;
    global $allmatches;
    global $originalToLocal;
    global $localToAbsolute;

    
   // echo var_dump($allmatches);
    
    echo "\n WORKING ON " . $url . "\n";
    
    
   // pause();


    $output = file_get_contents($url);
    
    writeToFile($output, $allmatches[$url]);
        
    // get all the urls from this page
    preg_match_all('/(?:src|href)=\"(.*)\"/iU', $output, $matches);
    //preg_match_all('/href=\"(.*)\"/iU', $output, $matches2);
   // $matches = array_merge($matches1, $matches2);
    //preg_match_all('/src=\"(.*)\"|href=\"(.*)\"/iU', $output, $matches);
    //preg_match_all('/src=\"(.*)\"/iU', $output, $srcmatches);
    //$matches = array_merge($hrefmatches, $srcmatches);

    
    // loop the links in this file and recurse
    foreach ($matches[1] as $m) {
    //        echo "\n REL TO ABS" . $url . " " . $m . " \n";
            if (empty($m)){
                continue;
            }
            $originalAddress = $m;
            $m = str_replace('www.newcastle.edu.au','www-test.newcastle.edu.au',$m);
        $m = rel2abs($m, $url);
        $m = str_replace('#'.parse_url($m,PHP_URL_FRAGMENT),'',$m);
    //        echo "\n REL TO ABS2" . $url . " " . $m . " \n";
            
        //    pause();
            echo "\n It's a " . $m . "\n";
                    pause();
                if (preg_match("/\.js$/i",$m)) {
                    echo "\n It's a JS " . $m . "\n";
                    pause();
                }
            
            
        // skip if not under starturl, is mailto, 
        if (0 === stripos($m, 'mailto')) {
            echo "\n Starts with Mailto \n";
            continue;
        }
        if (!(0 === stripos($m, $starturl))) { // should catch off host addresses
            echo "\n This " . $m . " did not match starturl " . $starturl . "\n";
                if (preg_match("/\.js$/i",$m)) {
                    echo "\n It's a JS " . $m . "\n";
                    pause();
                }
            $localOffsitePath = getOffsiteFiles($m);
            
            $originalToLocal[$originalAddress] = $localOffsitePath;
            
            echo "\n SETTING originalToLocal " . $originalAddress . " to be " . $localOffsitePath . "\n";
            
           // pause();
            
            continue;
        }
        //    pause();
        if (array_key_exists($m,$allmatches)) {
            $originalToLocal[$originalAddress] = $allmatches[$m]; // there may be various forms of the original address pointing to the same place, eg: it might have been relative to root, or current, or absolute

            echo "\n already exists " . $m . "\n";

        }
        else {
            
            $allmatches[$m] = getLocalPath($m);
            $originalToLocal[$originalAddress] = $allmatches[$m];

            $localToAbsolute[$allmatches[$m]] = $m;

            echo "\n INNER RECURSE" . $m . "\n";

            echo "AGAIN";
         //   pause();
            //var_dump($originalToLocal);
            //pause();
            recursePages($m);
        }
    }
    
    echo "Got it";
};


    // http://stackoverflow.com/questions/4444475/transfrom-relative-path-into-absolute-url-using-php
        // multiauthored code: mikealeonetti. Originally jordanstephens who sourced it from a now broken link.
function rel2abs( $rel, $base )
{
    /* return if already absolute URL */
    if( parse_url($rel, PHP_URL_SCHEME) != '' )
        return( $rel );

    /* queries and anchors */
    if( $rel[0]=='#' || $rel[0]=='?' ) {
        $selfquery = str_replace('?'.parse_url($base,PHP_URL_QUERY),'',$base); //don't keep tacking the qs on the end of the qs, replace it.
        return( $selfquery.$rel );
    }

    /* parse base URL and convert to local variables:
       $scheme, $host, $path */
    extract( parse_url($base) );

    /* remove non-directory element from path */
    $path = preg_replace( '#/[^/]*$#', '', $path );

    /* destroy path if relative url points to root */
    if( $rel[0] == '/' )
        $path = '';

    /* dirty absolute URL */
    $abs = '';

    /* do we have a user in our URL? */
    if( isset($user) )
    {
        $abs.= $user;

        /* password too? */
        if( isset($pass) )
            $abs.= ':'.$pass;

        $abs.= '@';
    }

    $abs.= $host;

    /* did somebody sneak in a port? */
    if( isset($port) )
        $abs.= ':'.$port;

    $abs.=$path.'/'.$rel;

    /* replace '//' or '/./' or '/foo/../' with '/' */
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for( $n=1; $n>0; $abs=preg_replace( $re, '/', $abs, -1, $n ) ) {}

    /* absolute URL is ready! */
    return( $scheme.'://'.$abs );
}

function getLocalPath($address){
    global $outputPath;
    global $starturl;
    echo "\n !!!!!!!!!!!!!!!!!!!!!!!!!! \n getting local path " . $outputPath . " ~ " . $starturl . " ~ " . $address . " \n" ;
    //pause();

    $webpath = parse_url($address);
    $localpath = $webpath["path"];
    $rootpath = parse_url($starturl)["path"];
        echo "\n rootpath \n" . preg_quote($rootpath,'/');
    // remove the site root from the path, but leave the directories under it.
    
    
    $localpath = preg_replace("/".preg_quote($rootpath,'/')."/i", "", $localpath);
    
    if (!(strpos($localpath, "."))){
        $localpath = $localpath . "/index.html";
    }
    if (!(empty($webpath["query"]))){
        $query = "?".$webpath["query"];
    }
    $localpath = $outputPath."/".$localpath.$query;
    $localpath = preg_replace("~/+~","/",$localpath);
    echo "\n" . $localpath . " was path \n";
    //pause();
    return $localpath;
}

function writeToFile($text, $filepath){
    
    echo "\n PATH is " . $filepath . " \n";
    //pause();
    if(!file_exists(dirname($filepath))) {
        mkdir(dirname($filepath), 0777, true);
    }
    file_put_contents($filepath, $text);
    //pause();
}

function getOffsiteLocalPath($offsiteUrl) {
        global $outputPath;
    $css = true;
    $js = true;
    $img = true;
        
    if (preg_match("/\.js$/i",$offsiteUrl) && $js) {
        $folder = "/js";
        echo "\n JS " . $offsiteURL;
        pause();
    }
    elseif (preg_match("/\.css$/i",$offsiteUrl) && $css) {
        $folder = "/css";
    }
    elseif (preg_match("/(\.png|\.jpg|\.jpeg|\.gif)$/i",$offsiteUrl) && $img) {
        $folder = "/img";
    }
    else {
        return "";
    }
    $convertpath = $outputPath . $folder . parse_url($offsiteUrl)["path"];

    return $convertpath;
}

function getOffsiteFiles($offsiteUrl) {
    
    global $offsites;
    global $outputPath;
    $convertPath = getOffsiteLocalPath($offsiteUrl);
    
    echo "\n GET OFFSITE AND RETURN LOCAL ADDRESS \n" . $convertPath . "\n";
    
    if (array_key_exists($offsiteUrl,$offsites)) {
        return $convertPath;
    }

    
    echo "\n OFFSITE FILE ";
    if (!empty($convertPath)){
            $output = file_get_contents($offsiteUrl);
    //pause();
            writeToFile($output, $convertPath);
    }
            $offsites[$offsiteUrl] = $convertPath;
            return $convertPath;
    
}

// Post processing to make links in the files work.
function cleanLinks() {
        global $allmatches;
        global $offsites;
        global $originalToLocal;
        global $localToAbsolute;
    echo "\n CLEAN LINKS \n";
    pause();

    var_dump($originalToLocal);
    

    
    foreach ($allmatches as $key => $value) {
            $thispagematches = null;
        $content = file_get_contents($value);
        echo "\nCLEANING " . $value . "\n";
       // pause();
            // get all the urls from this page, and replace them with the local path.
        preg_match_all('/(?:src|href)=\"(.*)\"/iU', $content, $thispagematches);
//var_dump($thispagematches);
            foreach ($thispagematches[1] as $p) {
                echo "\n ORIG TO LOCALS " . $p . " ~ " . $originalToLocal[$p] . "\n";
                if (!empty($originalToLocal[$p])){
                    // if there is something to swap it with (as in it's not a link of to some totally different site)
                    // then swap the href and src elements with the matching local address, converted to the right path for the new web host, and tacking on the original #anchor bit.
                    
                    $content = preg_replace("/href=\"".preg_quote($p,'/')."/iU", "href=\"".getNewWebPath($p), $content);
                    $content = preg_replace("/src=\"".preg_quote($p,'/')."/iU", "src=\"".getNewWebPath($p), $content);
                }
            }
            file_put_contents($value, $content);
    }
}

function getNewWebPath($originalPath) {
    global $outputPath;
    global $newwebbasepath;
    global $originalToLocal;
    if (empty(parse_url($originalPath)[fragment])){
        $anchors = "";
    }
    else {
        $anchors = "#" . parse_url($originalPath)[fragment];
    }
    $localpath = $originalToLocal[$originalPath].$anchors;
    
    echo "\n getting new web path \n" . $localpath . " ~ " . $outputPath . " ~ " . $newwebbasepath . "\n";
   // pause();

    $newwebpath = preg_replace("/".preg_quote($outputPath,'/')."/i", "", $localpath);
    $newwebpath = $newwebbasepath."/".$newwebpath;
    $newwebpath = preg_replace("~index.html~","/",$newwebpath); // index.html is ending up in the middle of a url sometimes. Would be better to figure out why, but just to get this done...
    if (!(strpos($localpath, "."))){
        $localpath = $localpath . "/index.html"; //and tack it on the end again.
    }
    $newwebpath = preg_replace("~/+~","/",$newwebpath); //clear out double slashes
    echo $newwebpath . "\n";    
   // pause();
    return $newwebpath;
}

    function pause() {
            $stdin = fopen('php://stdin', 'r');
            $response = fgetc($stdin);
    }
        
        

?>